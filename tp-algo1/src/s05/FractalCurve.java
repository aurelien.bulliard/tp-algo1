package s05;


// import proglib.TurtlePanel;

public class FractalCurve {

  private static final int L = 0;  // Code meaning TurnLeft
  private static final int R = 1;  // Code meaning TurnRight
  private static final int A = 3;  // Code meaning Advance
  private static final int C = 4;  // Code meaning Circle

  private static final int panelWidth  = 800;
  private static final int panelHeight = 600;
  private static final int HMargin     =  30;
  private static final int startX      = 0;
  private static final int startY      = panelHeight;
  
  private static int maxLevels =  40;
  
  private static int[] pattern = {A,A,L,A,A,R,A,A,R,A,A,L,A,A};
  
  private TurtlePanel  turtle; 
  private double       scaleFactor = Math.pow(getStraightAdvance(), maxLevels);
  private int          usableWidth = (panelWidth-HMargin);
  private float        baseDist    = (float)(usableWidth/scaleFactor)+(float) 0.5;
  
  //----------------------------------------------------------------------------
  public FractalCurve() {
    turtle = TurtlePanel.create("Fractal Curve [level = " + maxLevels + "]",
                                panelWidth, panelHeight,
                                startX,     startY     , true);
    drawPattern(maxLevels);
  }

  //----------------------------------------------------------------------------
  public void pause(long time) {
    try {
      Thread.sleep(time);
    } catch( InterruptedException e ) { }
  }

  //----------------------------------------------------------------------------
  // you can use turtle.advance(baseDist), and maybe pause(500)
  public void drawPattern(int level) {
    if (level > 0){
      for (int direction : pattern) {
        switch (direction){
          case A:
            //Advance
            if (level == 1){
              turtle.advance(baseDist);
            } else {
              drawPattern(level-1);
            }
            break;
          case L:
            //Go left
            turtle.turnLeft();
            break;
          case R:
            //Go right
            turtle.turnRight();
            break;
          case C:
            //Draw a circle
            turtle.drawCircle(10, false);
            break;
        }
      }
    }
  }
  
  //----------------------------------------------------------------------------
  // Get number of (advance) steps in initial direction
  //----------------------------------------------------------------------------
  private int getStraightAdvance() {
    int ix=1, iy=0;    // Initial direction (orientation vectors)
    int ctSteps=0;     // Count steps in initial direction
    
    for (int op : pattern) {
      if (op == A) {
        if      (ix ==  1) ctSteps++;  // Step backward
        else if (ix == -1) ctSteps--;  // Step forward
      } else if (op == L) {
        int tx = ix;
        ix = -iy;
        iy =  tx;
      } else if (op == R) {
        int tx = ix;
        ix =  iy;
        iy = -tx;
      }
    }
    return ctSteps;
  }
  
  //----------------------------------------------------------------------------
  public static void main(String[] args) {
    if (args.length == 1) maxLevels = Integer.parseInt(args[0]);
    new FractalCurve();
  }
}