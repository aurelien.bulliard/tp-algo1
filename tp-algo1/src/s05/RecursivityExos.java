package s05;
public class RecursivityExos {

  public static int factorial(int x) {
    return (x <= 1) ? x : x*factorial(x-1);
  }

  public static int modulo(int x, int y) {
      return (x-y<0) ? x : modulo(x-y,y);
  }

  public static int square(int n) {
    return (n < 1) ?  0 : square(n-1)+(2*n)-1;
  }

  public static int nbOf1sInBinary(int x) {
    return (x == 0) ? 0 : ((x % 2 == 1 ? 1 : 0) + nbOf1sInBinary(x/2));
  }

  public static int fibonacci(int n) {
    return (n == 0 || n == 1) ? n : fibonacci(n-1)+fibonacci(n-2);
  }
  // ---------------------------------------- 
  // ----------------------------------------
  public static void main(String[] args) {
    int n = 7;
    System.out.println(modulo(n, 6));    // 1
    System.out.println(factorial(n));       // 5040
    System.out.println(nbOf1sInBinary(n));  // 3
    System.out.println(fibonacci(n));       // 13
    System.out.println(square(n));          // 49
  }
}
