
package s02;

public class S02Ex4 {

  static int h(int[] t) {
    int n = t.length, r = n;
    if(n < 2) return n;
    for(int i=0; i<n; i++) {
      int c = 0;
      for(int e: t) 
        if(e == t[i]) c++;
      if(c == 1) return c;
      if(c < r) r = c;
    }
    return r;
  }

  public static void main(String[] args) {
    int[][] tt = {{2,2,5,2,5,5,5}, {4,3,5,6,7,5,4,5,2}};
    for(int[] t: tt) 
      System.out.println(h(t));
  }
}
