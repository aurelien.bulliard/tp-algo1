package s10;

public class ObjQueue {

        private class QueueNode {
            final Object elt;
            QueueNode next = null;
            // ----------
            QueueNode(Object elt) { this.elt = elt; }
        }
        private QueueNode front;
        private QueueNode back;
        public ObjQueue(){}
        public void enqueue(Object elt){
            QueueNode newadd = new QueueNode(elt);
            if(front == null){
                front = newadd;
            }else {
                back.next = newadd;
            }
            back = newadd;

        }
        public boolean isEmpty(){
            return back==null;
        }
        // PRE : !isEmpty()
        public Object consult(){
            return front.elt;
        }
        public Object dequeue(){
            Object e = front.elt;
            if (front == back) {
                back = null; front = null;
            } else {
                front = front.next;
            }
            return e;
        }


}
