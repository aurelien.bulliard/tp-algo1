package s10;

public class QueueChained <E> {
    private class QueueNode {
        final E elt;
        QueueNode next = null;
        // ----------
        QueueNode(E elt) { this.elt = elt; }
    }
    private QueueNode front;
    private QueueNode back;
    public QueueChained(){}
    public void enqueue(E elt){
        QueueNode newadd = new QueueNode(elt);
        System.out.println("enquee :"+elt);
        if(front == null){
            front = newadd;
        }else {
            back.next = newadd;
        }
        back = newadd;

    }
    public boolean isEmpty(){
        return back==null;
    }
    // PRE : !isEmpty()
    public E consult(){
        return front.elt;
    }
    public E dequeue(){
        E e = front.elt;
        System.out.println("dequee :"+e);
        if (front == back) {
            back = null; front = null;
        } else {
            front = front.next;
        }
        return e;
    }

}

