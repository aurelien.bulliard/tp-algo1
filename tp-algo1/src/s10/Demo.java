package s10;
public class Demo {
  static void demo(int n) {
    QueueChained<Integer> f;
    int i, sum=0;
    f = new QueueChained<>();
    for (i=0; i<n; i++)
      f.enqueue(i);
    while(! f.isEmpty())
      sum = sum + f.dequeue();
    System.out.println(sum);
  }
  static void demoObject(int n) {
    ObjQueue f;
    int i, sum=0;
    f = new ObjQueue();
    for (i=0; i<n; i++){
      f.enqueue(i);
      System.out.println("Entrée : " + i);
    }
    while(! f.isEmpty())
      System.out.println("Sortie : " + f.dequeue());
  }
  public static void main(String[] args){
    System.out.println("=== <Integer> ===");
    demo(10);
    System.out.println("=== Object Integer ===");
    demoObject(10);
  }
}
