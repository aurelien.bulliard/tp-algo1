package s04;

import java.util.Random;

public class Sorting {
  public static void main(String [] args) {
    int[] t = {4, 3, 2, 6, 8, 7};
    int[] u = {2, 3, 4, 6, 7, 8};
    insertionSort(t);
    for (int i=0; i<t.length; i++)
      if(t[i] != u[i]) {
        System.out.println("Something is wrong..."); return;
      }
    int[] v = {5};
    insertionSort(v);
    int[] w = {};
    insertionSort(w);
    //My tests ========================
    int[] rand = new int[10];
    Random ran = new Random();
    System.out.print("Initial array: ");
    for (int i = 0; i < rand.length; i++){
      //Filling this array with random numbers from 0 to 100
      rand[i] = ran.nextInt(100);
      System.out.print(rand[i]+" ");
    }
    System.out.println("");
    //Sorting with selection
    int[] randorg = rand; //Copy of the unsorted array
    System.out.print("Selection sort: ");
    selectionSort(rand);
    for (int nb : rand) {
      System.out.printf(nb + " ");
    }
    System.out.println("");
    //Shell sort
    System.out.print("Shell sort: ");
    selectionSort(randorg);
    for (int nb : randorg) {
      System.out.printf(nb + " ");
    }
    // ===========================
    System.out.println("\nMini-test passed successfully...");
  }
  //------------------------------------------------------------
  public static void selectionSort(int[] a) {
    for (int i = 0; i < a.length; i++){
      int lowest = Integer.MAX_VALUE; //search the lowest number in the array
      int index = 0; //Index of the number in the array
      for (int j = i; j < a.length; j++){
        if (a[j] < lowest){
          lowest = a[j];
          index = j;
        }
      }
      a[index] = a[i]; //switch
      a[i] = lowest;
    }

    }

    //------------------------------------------------------------
    public static void shellSort(int[] a) {
      //find intial k
      int k = 0;
      while (k * 3 + 1 < a.length & k*3+1 > 0) {
        k = k * 3 + 1;
      }
      while (k > 0) { //Loop from k to 1
        for (int i = k; i < a.length; i++) { //Start from k, and go to the right 1 by 1
          int right = i; //The rightmost value of this iteration
          while (right - k >= 0) { //Until going k values to the left is out of bounds
            if (a[right] < a[right - k]) { //If our rightmost is smaller than our "k to the left"
              //Swap the two values
              int value = a[right];
              a[right] = a[right - k];
              a[right - k] = value;
              right -= k; //We try to go k values to the left
            } else {
              break; //Exit this loop
            }
          }
        }
        k = (k - 1) / 3; //Formula to get the next k
      }
    }

    //------------------------------------------------------------
    public static void insertionSort(int[] a) {
        int i, j, v;

    for (i=1; i<a.length; i++) {
      v = a[i];          // v is the element to insert
      j = i;
      while (j>0 && a[j-1] > v) {
        a[j] = a[j-1];   // move to the right
        j--;
      }
      a[j] = v;          // insert the element
    }
  }
  //------------------------------------------------------------
}
