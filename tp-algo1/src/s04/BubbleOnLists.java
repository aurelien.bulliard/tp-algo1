package s04;
import s03.List; 
import s03.ListItr; 

public class BubbleOnLists {
  //---------------------------------------------
  static void bubbleSortList(List l) {
    if (l.isEmpty()) return;
    ListItr li = new ListItr(l);
    int sorted = l.size(); //Position of the leftmost sorted item (Everything at his right has already been sorted)
    while(sorted != 0) { //Loop until we sorted the entire list
      boolean trucmuch = false;
      li.goToFirst(); //Go back to first
      for (int i = 0; i < sorted; i++){//Go to the value at his right until we reach the sorted part
        if (bubbleSwapped(li)){ //Swap is needed
          trucmuch = true; //Swap done
        }
      }
      if (!trucmuch) {
        break; //No swap done, the list is already sorted
      }
      sorted -= 1;
    }
  }
  //---------------------------------------------
  //Swaps between left and right element if needed
  //Returns true if swap occurred
  static boolean bubbleSwapped(ListItr li) {
    //check if the next one is the last:
    li.goToNext();
    if (li.isLast()) return false;
    li.goToPrev();
    boolean retour = false;
    //Define left and right values
    int left = li.consultAfter();
    li.goToNext();
    int right = li.consultAfter();
    if(right < left){//If right is smaller than left -> Swap
      //Swap
      li.removeAfter();
      li.insertAfter(left);
      li.goToPrev();
      li.removeAfter();
      li.insertAfter(right);
      li.goToNext();
      retour = true; //Telling the swap has occured
    }
    return retour;
  }
  //---------------------------------------------
  public static void main(String[] args) {
    List l=new List();
    ListItr li = new ListItr(l);
    int[] t = {4,3,9,2,1,8,0};
    int[] r = {0,1,2,3,4,8,9};
    for(int i=0; i<t.length; i++) {
      li.insertAfter(t[i]); li.goToNext();
    }
    bubbleSortList(l);
    li=new ListItr(l);
    for (int i=0; i<r.length; i++) {
      if (li.isLast() || li.consultAfter() != r[i])  {
        System.out.println("Oups... something is wrong");
        System.exit(-1);
      }
      li.goToNext();
    }
    if (!li.isLast()) {
      System.out.println("Oups... too much elements");
      System.exit(-1);
    }
    System.out.println("Test passed successfully");
  }
}