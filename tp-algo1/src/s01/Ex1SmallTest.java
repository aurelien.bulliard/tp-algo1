
package s01;

import java.util.ConcurrentModificationException;
import java.util.Enumeration;

public class Ex1SmallTest {
  public static void main(String[] args) {
    somePushAndPop();
    withPossibleExpansion();
    withTopDownTraversal();
    invalidatedEnumerationEx1b();
    System.out.println("Small test for s01.ex1 passed successfully!");
  }

  static void somePushAndPop() {
    CharStack st = new CharStack();
    checkTrue(st.isEmpty());
    char[] t = {'h', 'e', 'y'};
    for(char c: t) {
      st.push(c);
      checkFalse(st.isEmpty());
      checkEquals(c, st.top());
      checkEquals(c, simulatedTop(st));
    }
    for(int i=t.length-1; i>=0; i--)
      checkEquals(t[i], st.pop());
    checkTrue(st.isEmpty());
  }

  static void withPossibleExpansion() {
    int initialSize = 2;
    int size = initialSize * 5;
    CharStack st = new CharStack();
    for(int i=0; i<size; i++) 
      st.push((char)('B' + i));
    char c = st.pop();
    while(!st.isEmpty()) {
      char e = st.top();
      checkEquals(e, st.pop());
      checkEquals(e, c-1);
      c = e;
    }
    checkEquals('B', c);
  }
  
  static void withTopDownTraversal() {
    CharStack st = new CharStack();
    char[] t = {'h', 'e', 'y'};
    checkEquals(0, sizeFromEnumerator(st.topDownTraversal()));
    for(int i=0; i<t.length; i++) {
      st.push(t[i]);
      checkEquals(i+1, sizeFromEnumerator(st.topDownTraversal()));
    }
    Enumeration<Character> en = st.topDownTraversal();
    for(int i=t.length-1; i>=0; i--)
      checkEquals(t[i], en.nextElement());
  }
  
  static void invalidatedEnumerationEx1b() {
    CharStack st = new CharStack();
    char[] t = {'h', 'e', 'y'};
    Enumeration<Character> en0 = st.topDownTraversal();
    for(char c: t) st.push(c);
    Enumeration<Character> en3 = st.topDownTraversal();
    en3.nextElement();
    st.push('a');  // now en3 should also be "invalid"
    checkThrows(ConcurrentModificationException.class, () -> en0.hasMoreElements());
    checkThrows(ConcurrentModificationException.class, () -> en3.hasMoreElements());
    checkThrows(ConcurrentModificationException.class, () -> en0.nextElement());
    checkThrows(ConcurrentModificationException.class, () -> en3.nextElement());
  }
    
  static char simulatedTop(CharStack st) {
    char c = st.pop();
    st.push(c);
    return c;
  }
  
  static int sizeFromEnumerator(Enumeration<?> en) {
    int s = 0; 
    while(en.hasMoreElements()) {
      s++;
      en.nextElement();
    }
    return s;
  }
  
  //-------------------------------------------------------------
  // Some testing utilities
  
  private static void checkEquals(char expected, char observed) {
    checkTrue(expected == observed);
  }

  private static void checkEquals(int expected, int observed) {
    checkTrue(expected == observed);
  }
  
  private static void checkFalse(boolean b) {
    checkTrue(!b);
  }
  
  private static void checkTrue(boolean b) {
    if(!b) throw new Error();
  }
  
  private static void checkThrows(Class<?> clazz, Runnable r) {
    try {
      r.run();
      throw new Error("should have thrown " + clazz.getName()); 
    } catch(Throwable e) {
      checkTrue(e.getClass() == clazz);
      // expected behavior, nothing to do
    }
  }
}
