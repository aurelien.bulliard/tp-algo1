package s01;

import java.nio.Buffer;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public class CharStack {
  private char[] buffer;
  private int topIndex;
  private boolean isEnumValid;

  private static final int DEFAULT_SIZE = 10;

  /**
   * CharStack's constructor
   * Creates a new instance of CharStack, with a given estimated size.
   * Size must be > 1, or the method will return an error.
   * @param estimatedSize The estimated size of the stack
   */
  public CharStack(int estimatedSize) {
    buffer = new char[estimatedSize];
    topIndex = -1;
    isEnumValid = true;
  }

  /**
   * CharStack's constructor
   * Creates a new instance of CharStack, with a default size of 10.
   */
  public CharStack() {
    this(DEFAULT_SIZE); 
  }

  /**
   * isEmpty()
   * @return true if the stack is empty, false otherwise.
   */
  public boolean isEmpty() {
    return topIndex < 0 ; //Default value for char == empty
  }

  /**
   * top()
   * Stack must not be empty or the method will return an arrayOutOfBoundException.
   * @return the top element of the stack.
   */
  public char top() {
    return buffer[topIndex];
  }

  /**
   * pop()
   * Pops out the top element of the stack
   * The stack must not be empty or the method will return an arrayOutOfBoundsException.
   * @return the item on top of the stack
   */
  public char pop() {
    char chatAtTop = buffer[topIndex];
    topIndex--;
    isEnumValid = false;
    return chatAtTop;
  }

  /**
   * push()
   * pushes a new item to the stack, doubling its size if full
   * @param x char to add on top of the stack
   */
  public void push(char x) {
    if (topIndex == buffer.length-1){
      //buffer is full -> Doubling the size
      char[] tempbuffer = new char[buffer.length*2];
      for (int i = 0; i < buffer.length; i++){
        tempbuffer[i] = buffer[i];
      }
      buffer = tempbuffer;
    }
    topIndex++;
    buffer[topIndex] = x;
    isEnumValid = false;
  }
  
  /* Typical usage: 
   *   Enumeration<Character> en = myStack.topDownTraversal();
   *   while(en.hasMoreElements()) 
   *     System.out.println(en.nextElement());
   * Note that that according to the specification, nextElement() 
   * has to throw a NoSuchElementException
   */
  public Enumeration<Character> topDownTraversal() {
    isEnumValid = true; //Enum just been created, he's valid for the moment
    Enumeration<Character> e = new Enumeration<Character>() {
      int elements = topIndex+1; // number of elements currently

      /**
       * hasMoreElements
       * Tells if the enumeration has more elements.
       * @return true if the enum has more elements
       * @throws ConcurrentModificationException If the stack has been modified by a pop() or a push() previously
       */
      @Override
      public boolean hasMoreElements() {
        if (!isEnumValid){throw new ConcurrentModificationException();}
        return elements > 0;
      }
      /**
       * nextElement
       * Doesn't affect the stack (it doesn't remove the top element when used)
       * @return the next char in the stack
       * @throws ConcurrentModificationException If the stack has been modified by a pop() or a push() previously
       */
      @Override
      public Character nextElement() {
        if (!isEnumValid){throw new ConcurrentModificationException();}
        elements--;
        return buffer[elements];
      }
    };
    return e;
  }

}
