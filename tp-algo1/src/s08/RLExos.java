package s08;
public class RLExos {
  //----------------------------------------------------------------
  //--- Exercises, S08 :
  //----------------------------------------------------------------
  /* append: adds an element at the end of the list. 
             append("abcd", 'z') gives "abcdz"                    */
              
  public static CharRecList append(CharRecList l, char e) {
    if(l.isEmpty())
    {
      return l.withHead(e);
    }
     return append(l.tail(),e).withHead(l.head());
  }
  //----------------------------------------------------------------
  /* concat: concatenates two lists. 
             concat("abcd", "xyz") gives "abcdxyz"               */
  public static CharRecList concat(CharRecList l, CharRecList r) {
    if(l.isEmpty())
    {
      return r;
    }
    return concat(l.tail(),r).withHead(l.head());
  }
  //----------------------------------------------------------------
  /* replaceEach : changes every occurrence of a certain element. 
              replaceEach("bcabcaxy", 'a', 'b') gives "bcbbcbxy"  */
  public static CharRecList replaceEach(CharRecList l, char old, char by) {
    if(l.isEmpty()){
      return l;
    }
    if(l.head()== old){
      return replaceEach(l.tail(),old,by).withHead(by);
    }
      return replaceEach(l.tail(),old,by).withHead(l.head());
  }
  //----------------------------------------------------------------
  /* consultAt: returns the element at index i (counting from zero). 
                consultAt("abcde", 3) gives 'd'                   */
  public static char consultAt(CharRecList l, int index) {
    if (index == 0){
      return l.head();
    }
    return consultAt(l.tail(), index-1);
  }
  //----------------------------------------------------------------
  /* isEqual: whether both lists have the same content (in same order). 
              isEqual("abc", "abc")  is true
              isEqual("abc", "bca")  is false
              isEqual("abc", "abcd") is false                     */
  public static boolean isEqual(CharRecList a, CharRecList b) {
    if(a.isEmpty()&b.isEmpty()){
      return true;
    }
    if(a.head()==b.head()){
      return isEqual(a.tail(),b.tail());
    }
    return false;
  }
  //----------------------------------------------------------------
  /* isSuffix: whether suff is a suffix of l. 
               isSuffix("abcd", "bcd")  is true
               isSuffix("abcd", "bc")   is false
               isSuffix("abcd", "")     is true
               isSuffix("abcd", "abcd") is true                   */
  public static boolean isSuffix(CharRecList l, CharRecList suff) {
    if (l.isEmpty()){
      return false;
    }
    if (isEqual(l.tail(),suff)){
      return true;
    }
    return isSuffix(l.tail(), suff);

  }

  //----------------------------------------------------------------
  //--- Examples from course slides:
  //----------------------------------------------------------------
  public static int sizeOf(CharRecList l) {
    if (l.isEmpty()) 
      return 0;
    return 1 + sizeOf(l.tail());
  }
  //----------------------------------------------------------------
  public static CharRecList inverse(CharRecList l) {
    if (l.isEmpty()) 
      return l;
    return append(inverse(l.tail()), l.head());
  }
  //----------------------------------------------------------------
  public static boolean isMember(CharRecList l, char e) {
    if (l.isEmpty()) 
      return false;
    if (e == l.head())
      return true;
    return isMember(l.tail(), e);
  }
  //----------------------------------------------------------------
  public static CharRecList smaller(CharRecList l, char e) {
    if (l.isEmpty()) 
      return l;
    if ( l.head() < e )
      return smaller(l.tail(), e).withHead(l.head());
    return smaller(l.tail(), e);
  }
  //----------------------------------------------------------------
  public static CharRecList greaterOrEqual(CharRecList l, char e) {
    if (l.isEmpty()) 
      return l;
    if ( l.head() < e )
      return greaterOrEqual(l.tail(), e);
    return greaterOrEqual(l.tail(), e).withHead(l.head());
  }
  //----------------------------------------------------------------
  public static CharRecList quickSort(CharRecList l) {
    CharRecList left, right;
    if (l.isEmpty()) 
      return l;
    left  =        smaller(l.tail(), l.head());
    right = greaterOrEqual(l.tail(), l.head());
    left  = quickSort(left);
    right = quickSort(right);
    return concat(left, right.withHead(l.head()));
  }
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  //----------------------------------------------------------------
  public static void main(String [] args) {
    CharRecList l = CharRecList.EMPTY_LIST;
    CharRecList m = CharRecList.EMPTY_LIST;
    CharRecList test = CharRecList.EMPTY_LIST;
    l = l.withHead('c').withHead('d').withHead('a').withHead('b');
    m = m.withHead('t').withHead('u').withHead('v');
    test = test.withHead('c').withHead('d');
    System.out.println("list l : "+ l);
    System.out.println("list m : "+ m);

    System.out.println( "quickSort(l) : "+           quickSort(l)   );
    System.out.println( "append(l,'z') : "+          append(l,'z')  );
    System.out.println( "concat(l,m) : "+            concat(l,m)    );
    System.out.println( "replaceEach(l,'a','z') : "+ replaceEach(l,'a','z') );
    System.out.println( "consultAt(l,2) : "+         consultAt(l,2) );
    System.out.println("isequal(l,l) :" + isEqual(l,l));
    System.out.println("isequal(l,m) :" + isEqual(l,m));
    System.out.println( "consultAt(l,2) : "+         consultAt(l,2) );
    System.out.println( "isSuffix(l, dc) : "+         isSuffix(l, test));
    //...
  }


}
