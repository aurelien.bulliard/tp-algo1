package s15;
import java.util.Random;
import java.util.Queue;
import java.util.LinkedList;

public class ExoBTree {

  public static String breadthFirstQ(BTNode t) {
    String res="";
    Queue<BTNode> q = new LinkedList<BTNode>();
    q.add(t);
    while(! q.isEmpty()) {
      BTNode crt = q.remove(); 
      if (crt==null) continue;
      res += (" "+crt.elt); 
      q.add(crt.left );
      q.add(crt.right);
    }
    return res;
  }

  public static int size(BTNode t) {
    return size(t ,0);
  }
  public static int size(BTNode t,int s)
  {
    s++;
    if(t.left != null)
    {
      s =size(t.left,s);
    }
    if(t.right != null)
    {
      s = size(t.right,s);
    }
    return s;
  }

  public static int height(BTNode t) {
    return height(t ,0);
  }
  public static int height(BTNode t,int h) {
    h++;
    int hLeft = h;
    int hRight = h;
    if(t.left != null)
    {
      hLeft =height(t.left,h);
    }
    if(t.right != null)
    {
      hRight = height(t.right,h);
    }
    return Math.max(hLeft,hRight);
  }

  public static String breadthFirstR(BTNode t) {
    String result = "";
    for (int i = 0; i < height(t); i++) {
      result += visitLevel(t, i);
    }
    return result;
  }

  public static String visitLevel(BTNode t, int level) {
    if (level == 0){
      return " "+t.elt.toString();
    }
    String right = "";
    String left = "";
    //Dive left
    if(t.left !=null ){
      left  = visitLevel(t.left, level-1);
    }
    //Dive right
    if (t.right != null) {
      right = visitLevel(t.right, level-1);
    }
    return left + right;
  }

  public static void depthFirst(BTNode t) {
    if (t==null) return;
    System.out.print(" "+t.elt);
    depthFirst(t.left );
    depthFirst(t.right);
  }

  public static void rotateRight(BTNode y) {
    if(y == null || y.left == null)return;
    BTNode x = y.left;
    y.left = x.right;
    if(x.right != null)
      x.right.parent = y;

    if (y.parent != null) {
      if (y.parent.left == y)
        y.parent.left = x;
      else if (y.parent.right == y)
        y.parent.right = x;
    }

    x.parent = y.parent;
    y.parent = x;
    x.right = y;
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  private static Random rnd = new Random();
  // ------------------------------------------------------------
  public static BTNode rndTree(int size) {
    if (size==0) return null;
    BTNode root = new BTNode(Integer.valueOf(0), null, null, null);
    BTNode t=root;
    boolean isLeft;
    for(int i=1; i<size; i++) {
      t=root;
      while(true) {
        isLeft=rnd.nextBoolean();
        if (isLeft){
          if (t.left ==null) break;
          t=t.left;
        }else{
          if (t.right==null) break;
          t=t.right;
        }
      }
      BTNode newLeaf = new BTNode(Integer.valueOf(i), null, null, t);
      if (isLeft) t.left =newLeaf;
      else        t.right=newLeaf;
    }
    return root;
  }
  // ------------------------------------------------------------
  public static void main(String[] args) {
    int nbOfNodes = 10;
    BTNode t = rndTree(nbOfNodes);
    System.out.println("Tree:" +t);
    System.out.println(t.toReadableString());
    System.out.println("\nDepth first (recursive), preorder:");
    depthFirst(t); 
    System.out.println("\nBreadth first:");
    System.out.println(breadthFirstQ(t));
    System.out.println("\nBreadth first bis:");
    System.out.println(breadthFirstR(t));
    System.out.println("\nSize:" + size(t));
    System.out.println("\nHeight:" + height(t));
    System.out.println("\nElts at 2: "+visitLevel(t, 2));
    rotateRight(t.left);
    System.out.println(t.toReadableString());
  }
  // ------------------------------------------------------------
}

