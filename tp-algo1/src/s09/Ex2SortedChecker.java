package s09;

public class Ex2SortedChecker {
  public static boolean isSortingResultCorrect(int[] orig, int[] sorted) {
    //Is the same length ?
    if (orig.length != sorted.length){
      return false;
    }
    //Each element is here the same numbe of times each side ?
    for (int element : orig) {
      if(nbOfOccurrences(orig, element) != nbOfOccurrences(sorted, element)){
        return false;
      }
    }
    //Is sorted in the right order ?
    for (int i = 1; i < sorted.length; i++) {
      if(sorted[i] < sorted[i-1]){
        return false;
      }
    }
    return true;
  }

  // Maybe you'll find such a method useful...

  private static int nbOfOccurrences(int[] t, int e) {
    int nbOfOccurrences = 0;
    for (int i = 0; i < t.length; i++) {
      if (t[i] == e){
        nbOfOccurrences++;
      }
    }
    return nbOfOccurrences;
  }

  // ------------------------------------------------------------

  record ArrayPair(int[] orig, int[] sorted) {}
  
  public static void main(String[] args) {
    // TODO: add some more test cases
    
    ArrayPair[] trueInstances = {
        new ArrayPair(new int[]{4,9,2}, new int[]{2,4,9})
    };
    ArrayPair[] falseInstances = {
        new ArrayPair(new int[]{4,9,2}, new int[]{2,4,4}),
        new ArrayPair(new int[]{4,9,2}, new int[]{4,9,2}),
        new ArrayPair(new int[]{4,9,2}, new int[]{2,2,4,4})
    }; 
    for(ArrayPair p: trueInstances) 
      if(!isSortingResultCorrect(p.orig(), p.sorted()))
          throw new RuntimeException("bad news... Should be true");
    for(ArrayPair p: falseInstances) 
      if(isSortingResultCorrect(p.orig(), p.sorted()))
        throw new RuntimeException("bad news... Should be false");
    System.out.println("Seems Ok!");
  }
}
