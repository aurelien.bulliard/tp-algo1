package s09;

import java.util.Arrays;

import static s09.Ex2SortedChecker.isSortingResultCorrect;

public class Ex3SortingTest {
    //----- Maybe you'll find that useful… -------
    @FunctionalInterface
    interface ISorting {
        void sort(int[] t);
    }

    static final ISorting[] algos = {
            BuggySorting::sort00, BuggySorting::sort01,
            BuggySorting::sort02, BuggySorting::sort03,
            BuggySorting::sort04, BuggySorting::sort05,
            BuggySorting::sort06, BuggySorting::sort07,
            BuggySorting::sort08, BuggySorting::sort09,
            BuggySorting::sort10, BuggySorting::sort11,
            Arrays::sort
    };

    // for(ISorting a: algos) {
    //   ...
    //   a.sort(t);
    //   ...
    // }

    // ------------------------------------------------------------
    public static void main(String[] args) {
        for (int i = 0; i < algos.length; i++) {
            System.out.println("==================== Algo "+i+" ====================");
            ISorting algo = algos[i];
            int[] case1 = null;//1 -> null array
            try {
                algo.sort(case1);
                System.out.println("Sorted incorrectly, algo " + i + " : Case 1");
            } catch (NullPointerException e) {
                //Expected, go ahead
            } catch (Exception e2){
                System.out.println("Exception "+e2+" in algo " + i + " : Case 1");
            }
            int[] case2 = new int[]{}; //2 -> Empty array
            int[] case2sorted = Arrays.copyOf(case2, case2.length);
            try {
                algo.sort(case2sorted);
                if (!Arrays.equals(case2sorted, case2)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 2");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 2");
            }

            int[] case3 = new int[]{1}; //3 -> Array with one element
            int[] case3sorted = Arrays.copyOf(case3, case3.length);;
            try {
                algo.sort(case3sorted);
                if (!isSortingResultCorrect(case3, case3sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 3");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 3");
            }

            int[] case4 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; //4 -> Array already sorted
            int[] case4sorted = Arrays.copyOf(case4, case4.length);
            try {
                algo.sort(case4sorted);
                if (!isSortingResultCorrect(case4, case4sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 4");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 4");
            }
            int[] case5 = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}; //5 -> Array reverse sorted
            int[] case5sorted = Arrays.copyOf(case5, case5.length);;
            try {
                algo.sort(case5sorted);
                if (!isSortingResultCorrect(case5, case5sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 5");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 5");
            }
            int[] case6 = new int[]{10, 1, 9, 2, 8, 8, 3, 7, 8, 6, 5}; //6 -> Unsorted array
            int[] case6sorted = Arrays.copyOf(case6, case6.length);;
            try {
                algo.sort(case6sorted);
                if (!isSortingResultCorrect(case6, case6sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 6");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 6");
            }
            int[] case7 = new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE}; //7 -> Array with extreme values
            int[] case7sorted = Arrays.copyOf(case7, case7.length);;
            try {
                algo.sort(case7sorted);
                if (!isSortingResultCorrect(case7, case7sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 7");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 7");
            }
            int[] case8 = new int[]{-14,-23,-1,Integer.MIN_VALUE,-3,-2837}; //8 -> Array with negative numbers
            int[] case8sorted = Arrays.copyOf(case8, case8.length);;
            try {
                algo.sort(case8sorted);
                if (!isSortingResultCorrect(case8, case8sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 8");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 8");
            }
            int[] case9 = new int[400]; //9 -> Huge array filled with zeros
            int[] case9sorted = Arrays.copyOf(case9, case9.length);;
            try {
                algo.sort(case9sorted);
                if (!isSortingResultCorrect(case9, case9sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 9");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 9");
            }
            int[] case10 = new int[]{1,2,3,3,3,4,5,6}; //10 -> Already sorted array with floor
            int[] case10sorted = Arrays.copyOf(case10, case10.length);;
            try {
                algo.sort(case10sorted);
                if (!isSortingResultCorrect(case10, case10sorted)) {
                    System.out.println("Sorted incorrectly, algo " + i + " : Case 10");
                }
            } catch (Exception e) {
                System.out.println("Exception "+e+" in algo " + i + " : Case 10");
            }
        }
    }
}
