package s07;

import java.util.Arrays;

public class Quicksort {
  public static void main(String [] args) {
    int[] t = {8, 7, 6, 4, 3,2,};
    int[] u = {2, 3, 4, 6, 7, 8};
    quickSort(t);
    for (int test: t) {
      System.out.print(test+",");
    }
    System.out.println();
    if(!Arrays.equals(t, u))
        throw new IllegalStateException("Oops. Something is wrong...");
    System.out.println("OK. Tiny test passed...");
  }
  //------------------------------------------------------------
  // chooses a pivot, and partitions the sub-array
  // Returns the final position of the pivot
  private static int partition(int[] t, int left, int right) {
    int k = left+1;
    for(int i = left+1;i<=right;i++){
        if(t[left]>t[i]){
          int temp= t[k];
          t[k] = t[i];
          t[i] = temp;
          k++;
        }
    }
    int temp = t[left];
    t[left]=t[k-1];
    t[k-1]=temp;
    return k-1;
  }

  private static void quickSort(int[] t, int left, int right) {
    if (left >= right)
      return;
    int p = partition(t, left, right);
    quickSort(t, left, p-1);
    quickSort(t, p+1, right);
  }

  public static void quickSort(int[] t) {
    quickSort(t, 0, t.length - 1);
  }
}
