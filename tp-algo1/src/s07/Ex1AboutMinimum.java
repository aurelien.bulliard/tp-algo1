package s07;
import java.util.Arrays;

public class Ex1AboutMinimum {

    public static int min1(int[] t) {
        return min1rec(t, 0, Integer.MAX_VALUE);
    }

    private static int min1rec(int[] t, int pos, int smallest) {
        int newsmallest = smallest;
        if (t[pos] < smallest) {
            //This is the smallest one yet
            newsmallest = t[pos];
        }
        if (pos == t.length - 1) {
            //At the end, exiting
            return newsmallest;
        }
        //Goto next one
        return min1rec(t, pos + 1, newsmallest);
    }

    public static int min2(int[] t) {
        return min2rec(t, 0, t.length-1);
    }

    private static int min2rec(int[] t, int left, int right) {
        if (left >= right){
            //One left
            return t[right];
        } else if (right-left ==1){
            //Two left
            return Math.min(t[left], t[right]);
        }
        int middle = left + (right-left) /2;
        int minvalLeft = min2rec(t, left, middle);
        int minvalRight = min2rec(t, middle+1, right);
        return Math.min(minvalRight, minvalLeft);
    }

    //-------------------------------------------------------------------------

    @FunctionalInterface
    interface MinFunction {
        int min(int[] t);
    }

    static void checkTestCase(int[] t, MinFunction mf) {
        int[] t1 = Arrays.copyOf(t, t.length);
        int observed = mf.min(t1);
        if (!Arrays.equals(t, t1))
            throw new IllegalStateException("The input array is modified!");
        int expected = Arrays.stream(t).min().getAsInt();
        if (observed != expected)
            throw new IllegalStateException("Bad result: " + observed
                    + " instead of " + expected + " in " + Arrays.toString(t));
    }

    private static void tinyMinTest() {
        int[][] samples = {
                {3, 4, 5},
                {5, 4, 3},
                {4, 3, 5},
                {-1, -9},
                {-9, -1},
                {8}
        };
        for (int[] u : samples) {
            checkTestCase(u, Ex1AboutMinimum::min1);
            checkTestCase(u, Ex1AboutMinimum::min2);
        }
        System.out.print("End of tiny test.");
    }

    public static void main(String[] args) {
        int[] t = {4, 3, 4, 6, 8, 120938, 1};
        System.out.println(min1(t));
        System.out.println(min2(t));
        tinyMinTest();
    }

}
