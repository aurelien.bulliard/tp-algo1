package s14;
public class GridPath {
  //============================================================
  // Min Path Problem : Recursive version
  //============================================================
  public static int minPath(int[][] t) {
    return minPath(t, t.length-1, t[0].length-1);
  }
  public static int minPath(int[][] t, int i, int j) {
    if (i <0 || j <0) return Integer.MAX_VALUE/2;
    if (i==0 && j==0) return t[0][0];
    int a = minPath(t, i-1, j  );
    int b = minPath(t, i,   j-1);
    if (b < a) a = b;
    return a + t[i][j];
  }
  //============================================================
  // Min Path Problem : Dynamic Programming version
  //============================================================
  public static int minPathDyn(int[][] t) {
    int[][] tabSol = new int[t.length][t[1].length];
    tabSol[0][0] = t[0][0];
    for(int i = 1 ; i < t[0].length; i++)
      tabSol[0][i] = tabSol[0][i-1]+t[0][i];
    for(int i = 1 ; i < t.length; i++)
      tabSol[i][0] = tabSol[i-1][0]+t[i][0];

    for(int i = 1;i<t.length;i++)
    {
      for(int j = 1;j<t[0].length;j++){
        int left = tabSol[i][j-1]+t[i][j];
        int up = tabSol[i-1][j]+t[i][j];
        tabSol[i][j] = Math.min(up,left);
      }
    }

    return tabSol[t.length-1][t[1].length-1];
  }
  //============================================================
  // Small Main
  //============================================================
  public static void main (String[] args) {
    int[][] t = {
        {9,13,1,2},
        {8,1,5,3},
        {2,1,1,1},
        {4,2,3,5},
        {1,2,1,1}
    };
    System.out.println(minPath(t));
    System.out.println(minPathDyn(t));
  }
}
