package s14;

import java.util.Arrays;

public class SubsetSum {
  //============================================================
  public static void main(String[] args) {
    int[] t = {5,8,3};
    int max = 20;
    System.out.println(Arrays.toString(t));
    System.out.println("Valid sums < " + max + " :");
    for (int i=0; i<max; i++) {
      boolean r = subsetSumDyn(t,i);
      if (r) 
        System.out.print(i + " ");
    }
  }
  //============================================================
  // Subset Sum Problem : Dynamic Programming version
  // PRE: k0 < 1000 and sum of ti < 1000
  //============================================================
  public static boolean subsetSumDyn(int[] t, int k0) {
    boolean[][] validSolutions = new boolean[t.length+1][1000];
    validSolutions[0][0] = true; //First line
    for (int line = 1; line < validSolutions.length; line++) {
        for (int col = 0; col < validSolutions[line-1].length; col++) {
          if (validSolutions[line-1][col]){
            validSolutions[line][col] = true;
            validSolutions[line][col+t[line-1]] = true;
            if (col+t[line-1] == k0){
              //k0 trouvé ! Inutile de chercher plus loin
              return true;
            }
          }
        }
    }
    return validSolutions[validSolutions.length-1][k0];
  }
}
