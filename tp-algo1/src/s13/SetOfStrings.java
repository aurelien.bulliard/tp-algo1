package s13;
import java.util.Arrays;
import java.util.BitSet;

public class SetOfStrings {
  static final int DEFAULT_CAPACITY = 5;
  int      crtSize;
  // three "parallel arrays" (alternative: one array HashEntry[], 
  // with String/int/boolean fields)
  String[] elt;
  int[]    total;
  BitSet   busy;
  // ------------------------------------------------------------
  public SetOfStrings ()  {
    this(DEFAULT_CAPACITY);
  }

  public SetOfStrings(int initialCapacity)  {
    initialCapacity = Math.max(2, initialCapacity);
    elt   = new String[initialCapacity];
    total = new    int[initialCapacity];
    busy  = new BitSet(initialCapacity);
    crtSize = 0;
  }

  int capacity() {
    return elt.length;
  }

  // Here is the hash function :
  int hashString(String s) {
    int h = s.hashCode() % capacity();
    if (h<0) h=-h;
    return h;
  }

  // PRE: table is not full
  // returns the index where element e is stored, or, if 
  // absent, the index of an appropriate free cell 
  // where e can be stored
  int targetIndex(String e) {
    /*
     * 1) hacher l'élément
     *
     * 2)tant que tout les élément avec ce hash code n'ont pas été vus
     *    tester le hash de tout les cases possédent qqc
     *    si même hash tester si même entrée si oui return
     *
     * 3)trouver prochaine case vide (attention pas dépasser tableau)
     */

    int initialh = hashString(e);
    int h = initialh;

    int tot = total[h];//search the number of element with the same hash

    while (tot > 0) { //all element with the same hash not fund
      h = busy.nextSetBit(h);//find next element

      if (h == -1) h = busy.nextSetBit(0);
      int htest = hashString(elt[h]);

      if (htest == initialh) { //test if same element
        if (e.equals(elt[h])) return h;
        tot--;
      }
      h = (h + 1) % capacity();// change index without exit of bounds
    }

    //find next empty index
    int nextEmpty = busy.nextClearBit(initialh);
    if (nextEmpty >= capacity())
      nextEmpty = busy.nextClearBit(0);
    return nextEmpty;
  }



  public void add(String e) {
    if (crtSize*2 >= capacity()) 
      doubleTable();

    int h = targetIndex(e);


    if(busy.get(h)) return;//e is already in elt
    total[hashString(e)]++;
    elt[h] = e;
    busy.set(h);
    crtSize++;
  } 

  private void doubleTable() {
    String[] oldElt = elt;
    BitSet oldbusy = busy;
    crtSize = 0;

    total = new int[2* total.length];
    elt = new String[2* elt.length];
    busy = new BitSet(2* busy.length());

    //refill the array
    int eltindex = oldbusy.nextSetBit(0);
    while(eltindex != -1){
      add(oldElt[eltindex]);
      eltindex = oldbusy.nextSetBit(eltindex+1);
    }
  }

  public void remove(String e) {
    int i = targetIndex(e);
    if (!busy.get(i)) return; // elt is absent
    int h = hashString(e);
    total[h]--;
    elt[i]=null;
    busy.clear(i); 
    crtSize--;
  }

  public boolean contains(String e) {
    return busy.get(targetIndex(e));
  }
  
  public SetOfStringsItr iterator() {
    return new SetOfStringsItr() {
      private int index = 0;
      @Override
      public boolean hasMoreElements() {
        return busy.nextSetBit(index) >= 0;
      }

      @Override
      public String nextElement() {
        while(hasMoreElements() && !busy.get(index)){
          index++;
        }
        return elt[index++];
      }
    };
  }

  public void union (SetOfStrings s) {
    for (int i = 0; i < s.elt.length; i++) {
      String currentElt = s.elt[i];
      if(s.busy.get(i) && !contains(currentElt)){
        //Isn't in our array yet, adding...
        add(currentElt);
      }
    }
  }

  public void intersection(SetOfStrings s) {
    for (int i = 0; i < elt.length; i++) {
      String currentElt = elt[i];
      if (busy.get(i) && !s.contains(currentElt)){
        //Doesn't exist in s, removing...
        remove(currentElt);
      }
    }
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  private String[] arrayFromSet() {
    String[] t = new String[size()];
    int i=0;
    SetOfStringsItr itr = this.iterator();
    while (itr.hasMoreElements()) {
      t[i++] = itr.nextElement();
    }
    return t;
  }

  public String toString() { 
    SetOfStringsItr itr = this.iterator();
    if (isEmpty()) return "{}";
    String r = "{" + itr.nextElement();
    while (itr.hasMoreElements()) {
      r += ", " + itr.nextElement();
    }
    return r + "}";
  }
  // ------------------------------------------------------------
  // tiny demo
  // ------------------------------------------------------------
  public static void main(String [] args) {
    String a="abc";
    String b="defhijk";
    String c="hahaha";
    SetOfStrings s=new SetOfStrings();
    s.add(a); s.add(b); s.remove(a);
    if (s.contains(a) || s.contains(c) || !s.contains(b))
      System.out.println("bad news...");
    else
      System.out.println("ok");

    s.add(c); s.add(a);
    SetOfStringsItr itr = s.iterator();
    while (itr.hasMoreElements()){
      System.out.println(itr.nextElement());
    }
  }
}
