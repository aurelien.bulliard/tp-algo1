package s06;

import java.util.Arrays;

public class ShortToStringMap {
  //===================================================
  // A Java 'record' defines a class, here with two immutable fields
  // the corresponding getters key()/value(), and a reasonable 
  // implementation of the constructor, the equals() method
  // and a hashCode() method
  //===================================================
  //private record MapEntry(short key, String value) {} -- not in the example in the instructions

  private int size;
  private short[] keys;
  private String[] values;

  private ShortToStringMapItr iterator;
  //------------------------------
  //  Private methods
  //------------------------------
 
  // Could be useful, for instance :
  // - one method to detect and handle the "array is full" situation
  private boolean isFull(){
    return (keys.length==size);//(values[values.length-1] != null & keys[keys.length-1] != 0)
  }
  private void handleIfFull(){
    if (isFull()){
      keys = Arrays.copyOf(keys, keys.length+1);
      values = Arrays.copyOf(values, values.length+1);
    }
  }
  private void handleShrink(){
    if (keys.length-1 > size){//keys.length/2 >= size
      keys = Arrays.copyOf(keys, keys.length-1);
      values = Arrays.copyOf(values, values.length-1);
    }
  }
  // - one method to locate a key in the array
  private int whereIs(short key){
    int index = -1;
    for (int i = 0; i < size; i++){
      if (keys[i] == key){
        //Found
        index = i;
      }
    }
    return index;
  }
  //   (to be called from containsKey(), put(), and remove())

  //------------------------------
  //  Public methods
  //------------------------------
  public ShortToStringMap() {
    keys = new short[1];
    values = new String[1];
    size = 0;
    iterator = new ShortToStringMapItr() {
      int pos;
      @Override
      public boolean hasMoreKeys() {
        return pos < keys.length;
      }

      @Override
      public short nextKey() {
//        if (pos+1 >= keys.length){
//          //Loop back to the start
//          pos = 0;
//        }
        return keys[pos++];
      }
    };
  }

  // adds an entry in the map, or updates it
  public void put(short key, String val) {
    int location = whereIs(key);
    if (location < 0){
      //New value for a new key, check if full and handle if neccessary
      handleIfFull();
      values[size] = val;
      keys[size] = key;
      size++;
    } else {
      //Update, locate and change the value
      values[location] = val;
      keys[location] = key;
    }
  } 

  // returns null if !containsKey(key)
  public String get(short key) {
    int index = whereIs(key);
    return (index >=0) ? values[index] : null;
  }

  public void remove(short e) {
    if (size > 0){
      int pos = whereIs(e);
      if(pos >= 0){
        keys[pos] = keys[size-1];
        values[pos] = values[size-1];
        size--;
        handleShrink(); //Shrink the arrays if possible
      }
    }
  }

  public boolean containsKey(short k) {
    return whereIs(k)>=0;
  }

  public boolean isEmpty() {
    return size() == 0; 
  }
  
  public int size() {
    return size;
  }
  
  public ShortToStringMapItr iterator() {
    return iterator;
  }

  // a.union(b) :        a becomes "a union b"
  //  values are those in b whenever possible
  public void union(ShortToStringMap m) {
    for(int i = 0;i<m.size;i++){
      put(m.keys[i],m.values[i]);
    }
  }

  // a.intersection(b) : "a becomes a intersection b"
  //  values are those in b
  public void intersection(ShortToStringMap s) {
    int i = 0;
    while(i<s.size){
      if(!containsKey(s.keys[i])){
        s.remove(s.keys[i]);
      }
      else{
        i++;
      }
    }
    if(!containsKey(s.keys[i])) {
      s.remove(s.keys[i]);
    }
    keys = s.keys;
    values = s.values;
    size = s.size;

  }

  // a.toString() returns all elements in 
  // a string like: {3:"abc", 9:"xy", -5:"jk"}
  @Override public String toString() {
    String output = "{";
    for (short a : keys) {
      output += a+":\""+get(a)+"\", ";
    }
    if (output.length() > 2){
      output = output.substring(0, output.length()-2);
    }
    return output+"}";
  }
  // ------------------------------------------
  public static void main(String[] args) {
    // tiny demo
    ShortToStringMap sm = new ShortToStringMap();


    sm.put((short)4, "ab");
    sm.put((short)-8, "ef");
    sm.put((short)55, "xy");
    sm.put((short)12, "gh");
    sm.remove((short)55);
    sm.put((short)-8, "EF");
    System.out.println(sm);
  }

}
