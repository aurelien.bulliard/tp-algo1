package s06;
import java.util.Random;
import java.util.Arrays;

public class RandomTable {
  // ------------------------------------------------------------
  // PRE: m <= n, m >= 0
  // Returns: a sorted array with randomly chosen distinct elements in [0..n-1]
  public static short[] randomTable(short m, short n) {
    short[] result = new short[m];
    SetOfShorts set = new SetOfShorts();
    SetOfShortsItr itr = set.iterator();
    Random random = new Random();
    //Fill the set with random numbers
    while (set.size() != m){
      int r = random.nextInt(n);
      set.add((short) r);
    }
    //Fill our array with the contents of the set
    int i = 0;
    while (itr.hasMoreElements()){
      result[i] = itr.nextElement();
      i++;
    }
    //Sort
    Arrays.sort(result);
    return result;
  }
  // ------------------------------------------------------------
  static void testRandomTable(short m, short n, int nRepetitions) {
    String msg = "";
    for(int k=0; k<nRepetitions; k++) {
      short[] t = randomTable(m, n);
      msg = String.format("m=%d n=%d res=%s", m, n, Arrays.toString(t));
      if (m != t.length)
        throw new RuntimeException("Size of array is not correct: " + msg);
      if(m == 0) return;
      if (t[0]<0 || t[t.length-1] >= n)
        throw new RuntimeException("Elements must be in [0..n[" + msg);
      for (int i = 0; i < t.length - 1; i++) {
        if (t[i] >= t[i + 1])
          throw new RuntimeException(
                  "Array should be sorted and contain distinct numbers: " + msg);
      }
    }
    System.out.println("One sample: "+ msg);
    System.out.println("\nTest passed successfully !");
  }
  // ------------------------------------------------------------
  public static void main(String [] args) {
    short m = 10;
    short n = 50;
    if (args.length == 2) {
      m=Short.parseShort(args[0]);
      n=Short.parseShort(args[1]);
    }
    int nRepetitions = 100;
    testRandomTable(m, n, nRepetitions);
  }
}
