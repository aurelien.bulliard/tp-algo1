package s03;

// When isFirst(), it is forbidden to call goToPrev()
// When isLast(),  it is forbidden to call goToNext() 
// When isLast(),  it is forbidden to call consultAfter(), or removeAfter()
// For an empty list, isLast()==isFirst()==true
// For a fresh ListItr, isFirst()==true
// Using multiple iterators on the same list is allowed only 
// if none of them modifies the list

public class ListItr {
  final List list;
  ListNode pred, succ;
  // ----------------------------------------------------------
  public ListItr( List anyList ) {
    list = anyList; 
    goToFirst();   
  }

  public void insertAfter(int e) {
    ListNode a = new  ListNode(e,pred,succ);
    if(! isFirst()){
        pred.next = a;
    }else{
      list.first = a;
    }
    if (! isLast()){
        succ.prev = a;
    }else{
      list.last = a;
    }
    succ = a;
    list.size++;
  }

  public void removeAfter() {
      if (succ.next != null){
        succ.next.prev = pred;
      } else {
        list.last = pred;
      }

    if (!isFirst()){
      pred.next = succ.next;
    }else{
      list.first = succ.next;
    }
    succ = succ.next;
    list.size--;
  }

  public int  consultAfter() {
    return succ.elt;
  }
  public void goToNext() {
    pred = succ; 
    succ = succ.next; 
  }
  public void goToPrev() {
    succ = pred;
    pred = pred.prev; 
  }
  public void goToFirst() { 
    succ = list.first; 
    pred = null;
  }
  public void goToLast() { 
    pred = list.last;  
    succ = null;
  }
  public boolean isFirst() { 
    return pred == null;
  }
  public boolean isLast() { 
    return succ == null; 
  }
}

